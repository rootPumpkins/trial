﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    [SerializeField]
    NavMeshAgent agent;
    [SerializeField]
    GetPoint getPoint;
    [SerializeField]
    Squad squad;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            agent.SetDestination(getPoint.GetThatPoint().point);
        } else if (Input.GetMouseButtonDown(0))
        {
            Transform t = getPoint.GetThatPoint().transform.gameObject.transform;
            if (t.tag == "Enemy" || t.tag == "Squadman")
            {
                squad.SetTarget(t);
            } else
            {
                squad.SetTarget(null);
            }
        } else if (Input.GetButtonDown("1"))
        {
            squad.securePosition = false;
            squad.formUp = false;
            squad.searchNdestroy = true;
        } else if (Input.GetButtonDown("2"))
        {
            squad.securePosition = false;
            squad.searchNdestroy = false;
            squad.formUp = true;
        } else if (Input.GetButtonDown("3"))
        {
            squad.searchNdestroy = false;
            squad.formUp = false;
            squad.Recall();
            squad.securePosition = true;
        } else if (Input.GetButtonDown("4"))
        {
            squad.searchNdestroy = false;
            squad.formUp = false;
            squad.securePosition = false;
            squad.Recall();
        }
    }
}
