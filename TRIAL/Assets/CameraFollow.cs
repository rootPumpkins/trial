﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    Transform playerTransform;

    Vector3 offset;

    void Start()
    {
        offset = new Vector3(0f, 23f, -11.5f);
    }

    void Update()
    {
        transform.position = playerTransform.position + offset;
    }
}
