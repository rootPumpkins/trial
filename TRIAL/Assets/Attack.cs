﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Attack : MonoBehaviour
{
    [SerializeField]
    int attackDamage;
    [SerializeField]
    int maxHealth;
    int health;
    bool canAttack; // If true, will attack enemies within range, if false, will not
    [SerializeField]
    float attackRate;
    [SerializeField]
    float attackRange;

    public bool available; // If true, will return to formation at Form Up command, if false, will not

    [SerializeField]
    Squad squad;

    void Start()
    {
        health = maxHealth;
        canAttack = true;
    }

    void Update()
    {
        Transform closestEnemy = null;
        if (this.tag == "Squadman")
        {
            closestEnemy = squad.FindClosestEnemy(transform); // Reference to function that finds closest enemy for
//                                                               Squadman
        }
        else if (this.tag == "Enemy")
        {
            if (squad.transform.childCount < 1) // If there are no squadmen left
            {
                return;
            }
            closestEnemy = FindEnemyForEnemy(); // Reference to function that finds closest enemy for enemy
        }
        if (Vector3.Distance(transform.position, closestEnemy.position) < attackRange)
        {
            if (canAttack) // Only false if attack has already been made within the attackRate
            {
                DealDamage(closestEnemy.GetComponent<Attack>());
                canAttack = false;
                StartCoroutine("WaitToAttack");
            }
            available = false;
        } else
        {
            available = true;
        }
    }

    Transform FindEnemyForEnemy () // Function that finds the closest squadman
    {
        Transform _closestEnemy = squad.transform.GetChild(0);
        for (int i = 0; i < squad.transform.childCount; i++)
        {
            if (Vector3.Distance(transform.position, squad.transform.GetChild(i).position) < Vector3.Distance(transform.position, _closestEnemy.position))
            {
                _closestEnemy = squad.transform.GetChild(i);
            }
        }
        return _closestEnemy;
    }

    public void DealDamage (Attack subject) // Self explanitory
    {
        subject.TakeDamage(attackDamage);
    }
    public void TakeDamage (int dmg) // Self explanitory
    {
        health -= dmg;
        if (health < 1)
        {
            if (this.tag == "Squadman")
            {
                squad.agents.Remove(this.GetComponent<NavMeshAgent>()); // Removes component from list before
            } else if (this.tag == "Enemy") //                             being destroyed
            {
                squad.enemyList.Remove(this.transform);
            }
            GameObject.Destroy(this.gameObject);
        }
    }

    IEnumerator WaitToAttack ()
    {
        yield return new WaitForSeconds(attackRate);
        canAttack = true;
        StopCoroutine("WaitToAttack");
    }
}
