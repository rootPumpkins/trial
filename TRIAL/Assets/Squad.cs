﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Squad : MonoBehaviour
{

    Transform target;

    public bool searchNdestroy;
    public bool formUp;
    public bool securePosition;

    List<Vector3> offsetList = new List<Vector3>(); // Offset from the player position for formation

    public List<NavMeshAgent> agents = new List<NavMeshAgent>(); // Squadmen

    [SerializeField]
    GameObject enemies;

    [SerializeField]
    float protectRange; // Minimum range for an enemy to be deemed a threat to the player, and be attacked by
//                         squadmen on Secure Position command

    [SerializeField]
    Transform player;

    public List<Transform> enemyList = new List<Transform>(); // List of enemies for the squadmen to attack

    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            agents.Add(transform.GetChild(i).GetComponent<NavMeshAgent>()); // Fills the list
        }
        offsetList.Add(new Vector3(2f, 0f, 2f)); // Fills
        offsetList.Add(new Vector3(-2f, 0f, 2f)); // Another
        offsetList.Add(new Vector3(2f, 0f, -2f)); // List
        offsetList.Add(new Vector3(-2f, 0f, -2f)); // Here
        for (int i = 0; i < enemies.transform.childCount; i++)
        {
            enemyList.Add(enemies.transform.GetChild(i)); // Fills yet another list
        }
    }

    void Update()
    {
        if (searchNdestroy)
        {
            SearchNDestroy();
        } else if (formUp)
        {
            FormUp();
        } else if (securePosition)
        {
            Transform h = FindClosestEnemy(player);
            if (Vector3.Distance(player.position,h.position) < protectRange) // Is an enemy close to the player
            {
                for (int i = 0; i < agents.Count; i++)
                {
                    agents[i].SetDestination(h.position); // If so, attack him
                }
            } else
            {
                Recall(); // Go back to the player in formation
            }
        }
    }

    public Transform FindClosestEnemy (Transform subject) // Finds the closest enemy
    {
        Transform _closestEnemy = enemyList[0].transform;
        for (int i = 0; i < enemyList.Count; i++)
        {
            if(Vector3.Distance(subject.position,enemyList[i].position) < Vector3.Distance(subject.position,_closestEnemy.position))
            {
                _closestEnemy = enemyList[i];
            }
        }
        return _closestEnemy;
    }

    public void SetTarget (Transform _target)
    {
        target = _target;
    }

    public void FormUp () // Form up on the player
    {
        for (int i = 0; i < agents.Count; i++)
        {
            if (agents[i].GetComponent<Attack>().available)
            {
                agents[i].SetDestination(player.position + offsetList[i]);
            }
        }
    }

    public void Recall ()
    {
        for (int i = 0; i < agents.Count; i++)
        {
            agents[i].SetDestination(player.position + offsetList[i]);
        }
    }

    public void SearchNDestroy ()
    {
        if (enemyList.Count < 1) // If there are no enemies, then dont attack
        {
            searchNdestroy = false;
            return;
        }
        if (target != null) // If there is a target
        {
            if (target.tag == "Squadman")
            {
                target = null;
            } else
            {
                for (int i = 0; i < agents.Count; i++)
                {
                    agents[i].SetDestination(target.position); // Then, attack him
                }
            }
        }
        else // If not
        {
            for (int i = 0; i < agents.Count; i++)
            {
                Transform closestEnemy = FindClosestEnemy(agents[i].transform); // Find a nearby enemy
                agents[i].SetDestination(closestEnemy.position); // And attafck him
            }
        }
    }
}
